
document.querySelector(".button").addEventListener ("click", () => {
    let quetes = {
        '"willRogerz"': "'Don't Let Yesterday take up too much of today.'",
        '"-EarlNightingale"': '"We become what we think about."',
        '"چرچیل"': "طرز نگرش چیز کوچکی است که تغییر بزرگی ایجاد میکند."
    };
    let authors =Object.keys (quetes)
    let author = authors [Math.floor(Math.random() *authors.length)]
    let quete = quetes [author]
         
    document.querySelector(".quote").innerText = quete;
    document.querySelector(".author").innerText = author;
});
